# Gray

# D ---------0-0-0-0-
# A ---------0-0-0-0-
# F ---------7-7-7-7-
# C ---------5-5-5-5-
# D -----------------
# C -7--7--7---------

from pytube import YouTube, Playlist
import os, sys, re
import moviepy.editor as mp


def usage():
    print('Usage:', sys.argv[0], '[URL]', '<output_dir>', '\n\t-v: download video')
    exit(1)


try:
    output_dir = sys.argv[2]
except Exception:
    output_dir = 'files/'

try:
    pl_url = sys.argv[1]
except Exception:
    usage()

only_audio = '-v' not in sys.argv

playlist = Playlist(pl_url)
for url in playlist:
    YouTube(url).streams.filter(only_audio=only_audio).first().download(output_dir)

if only_audio:
    for file in os.listdir(output_dir):
        if re.search('mp4', file):
            mp4_path = os.path.join(output_dir, file)
            mp3_path = os.path.join(output_dir, os.path.splitext(file)[0] + '.mp3')
            new_file = mp.AudioFileClip(mp4_path)
            new_file.write_audiofile(mp3_path)
            os.remove(mp4_path)
